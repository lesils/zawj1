package servlets.tests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import servlets.HelloServlet;

@SuppressWarnings("unused")
public class TestHelloServlet extends Mockito{
	@Test
	public void servlet_rodzaj_rat__should_be_constant() {
		String rodzaj_rat = "constant";
		equals(1);
	}
	
	@Test
	public void servlet_should_not_greet_the_user_if_the_all_is_null() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
        when(request.getParameter("rodzaj_rat")).thenReturn(null);
        when(request.getParameter("wartosc_kredytu")).thenReturn(null);
        when(request.getParameter("liczba_rat")).thenReturn(null);
        when(request.getParameter("oprocentowanie")).thenReturn(null);
        when(request.getParameter("oplata_stala")).thenReturn(null);
     
        servlet.doGet(request, response);
        
        
        verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_return_rodzaj_rat() throws IOException {

		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		
		when(request.getParameter("rodzaj_rat")).thenReturn("1");
        when(response.getWriter()).thenReturn(writer);
		
        new HelloServlet().doGet(request, response);
        
        verify(writer).println("stala");
	}

	@Test
	public void servlet_should_not_greet_the_wartosc_kredytu_is_not_null() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		 
	
       
        servlet.doGet(request, response);
        assertEquals("text/html", response.getContentType());
        verify(writer).println("1000");
     
	}
	@Test
	public void servlet_should_not_greet_the_user_if_the_oplata_stala_is_null() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
     
        when(request.getParameter("oplata_stala")).thenReturn(null);
        servlet.doGet(request, response);
        
        verify(response).sendRedirect("/");
	}
	@Test
	public void servlet_should_not_greet_the_user_if_the_rodzaj_rat_is_null() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
        when(request.getParameter("rodzaj_rat")).thenReturn(null);
     
        servlet.doGet(request, response);
        
        
        verify(response).sendRedirect("/");
	}




}
