package servlets;


import java.io.IOException;

import java.io.Reader;
import java.io.StringReader;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;


@WebServlet("/hello")
public class HelloServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		int wartosc_kredytu = Integer.parseInt(request.getParameter("wartosc_kredytu"));
		int liczba_rat = Integer.parseInt(request.getParameter("liczba_rat"));
		int oprocentowanie = Integer.parseInt(request.getParameter("oprocentowanie"));
		int oplata_stala = Integer.parseInt(request.getParameter("oplata_stala"));
		String rodzaj_rat = request.getParameter("rodzaj_rat");
		

		if ((request.getParameter("wylicz") != null) || (request.getParameter("export") != null)) {
			if (  (Integer.parseInt(request.getParameter("liczba_rat")) > 0)
					|| (Integer.parseInt(request.getParameter("oprocentowanie"))>0) || (Integer.parseInt(request.getParameter("oprocentowanie"))<100)){
		StringBuilder builder = new StringBuilder();
		builder.append("<html>");
		builder.append("<head>");
		builder.append("<style> th, td {padding: 5px;text-align: left; border: 1px solid black;}</style>");
		builder.append("</head>");
		builder.append("<body>");		
		builder.append("<h1>Harmonogram splat</h1>");
		builder.append(createTable(wartosc_kredytu, liczba_rat, oprocentowanie, oplata_stala, rodzaj_rat));
		builder.append("</body>");
		builder.append("</html>");	

		Reader reader =  new StringReader(builder.toString());
		if(request.getParameter("export") != null){
			exportToPDF(request, response, reader);		
		}
		response.getWriter().print(builder);
		}
		else {
			response.sendRedirect("/");
		}
			
		}
	}

	
	public StringBuilder createTable
	(int wartosc_kredytu, int liczba_rat, double oprocentowanie, int oplata_stala, String rodzaj_rat){
		StringBuilder builder = new StringBuilder();
		
		builder.append("<table class='Harmonogram' style='border: 1px solid black;'>");
		builder.append("<tr>");
		builder.append("<th>Nr raty</th>");
		builder.append("<th>Kwota Kapitalu</th>");
		builder.append("<th>Kwota odsetek</th>");
		builder.append("<th>Oplaty stale</th>");
		builder.append("<th>Calkowita kwota raty</th>");
		builder.append("</tr>");
		int calosc_kredytu=wartosc_kredytu;
		double numer_raty = 1;
		double rate =
				wartosc_kredytu/liczba_rat +((oprocentowanie/100)/12);
				double stala_rata=liczba_rat;
				double sta=((((calosc_kredytu*oprocentowanie/100)+calosc_kredytu))/stala_rata);
		while (numer_raty <= liczba_rat){
			
			double kwota_odsetek = wartosc_kredytu * oprocentowanie/100;
		    builder.append("<tr>");
		    builder.append("<td>");
		    builder.append(numer_raty);
		    builder.append("</td>");
		    builder.append("<td> ");
		    builder.append(wartosc_kredytu);
		    builder.append("</td>");
		    builder.append("<td>");
		    builder.append(kwota_odsetek);
		    builder.append("</td>");
		    builder.append("<td>");
		    builder.append(oplata_stala);
		    builder.append("</td>");
		    builder.append("<td>");
		    if (rodzaj_rat.contains("1")){
		    	builder.append(rate+kwota_odsetek);
		    }
		    else {  	
		    	builder.append(sta);
		    	
		    }
		    builder.append("</td>");
		    builder.append("</tr>");
		    numer_raty += 1;
			wartosc_kredytu -= rate;
		}
		builder.append("</table>");
			
		return builder;
	}
	
	
	public void exportToPDF(HttpServletRequest request, HttpServletResponse response, Reader reader) throws IOException{
		response.setHeader("Content-Disposition", "attachment; filename=\"" + "pdf" + "\"");
		Document document = new Document();
        PdfWriter writer = null;
        System.out.println(response.getOutputStream());
		try {
			writer = PdfWriter.getInstance(document, response.getOutputStream());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        document.open();
        XMLWorkerHelper.getInstance().parseXHtml(writer, document, reader); 
        document.close();
	}


}
	
	